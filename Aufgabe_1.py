from __future__ import annotations
from dataclasses import dataclass

@dataclass
class Knoten:
    v: object = None # value
    next: Knoten = None

class MyStack:
    def __init__(self):
        self.head = None

    def push(self, a):
        self.head = Knoten(a, self.head)

    def pop(self):
        a = self.head.v
        self.head = self.head.next
        return a

    def isEmpty(self):
        return self.head is None

class IteratableDeque:
    def __init__(self):
        self._stack = []

    def push(self, a):
        self._stack.append(a)

    def pop(self):
        return self._stack.pop() if self._stack else None

    def isEmpty(self):
        return self._stack == []

    def pushFront(self, a):
        self._stack.insert(0,a)

    def popFront(self):
        return None if not self._stack else self._stack.pop(0)

    def __iter__(self):
        self.iter = iter(self._stack)
        return self

    def __next__(self):
        try:
            return next(self.iter)
        except StopIteration:
            raise StopIteration
